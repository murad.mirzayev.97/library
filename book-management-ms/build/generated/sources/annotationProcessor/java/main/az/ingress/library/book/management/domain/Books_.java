package az.ingress.library.book.management.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Books.class)
public abstract class Books_ {

	public static volatile SingularAttribute<Books, Integer> pageCount;
	public static volatile SingularAttribute<Books, String> publisherName;
	public static volatile SingularAttribute<Books, String> about;
	public static volatile SingularAttribute<Books, Long> id;
	public static volatile SingularAttribute<Books, String> bookName;

	public static final String PAGE_COUNT = "pageCount";
	public static final String PUBLISHER_NAME = "publisherName";
	public static final String ABOUT = "about";
	public static final String ID = "id";
	public static final String BOOK_NAME = "bookName";

}

