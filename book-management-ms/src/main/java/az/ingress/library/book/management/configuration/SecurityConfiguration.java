package az.ingress.library.book.management.configuration;

import az.ingress.library.common.configuration.BaseSecurityConfig;
import az.ingress.library.common.configuration.SecurityProperties;
import az.ingress.library.common.service.AuthService;
import az.ingress.library.common.service.JwtService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import java.util.List;

@Slf4j
@Import({SecurityProperties.class, JwtService.class})
@EnableWebSecurity
public class SecurityConfiguration extends BaseSecurityConfig {

    public SecurityConfiguration(SecurityProperties securityProperties, List<AuthService> authServices) {
        super(securityProperties, authServices);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/books/list")
                        .hasAnyAuthority("ROLE_ADMIN");

        super.configure(http);
    }
}
