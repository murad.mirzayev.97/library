package az.ingress.library.book.management.configuration;


import az.ingress.library.common.configuration.SecurityProperties;
import az.ingress.library.common.service.JwtService;
import az.ingress.library.common.service.TokenAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Slf4j
@Configuration
public class TokenAuthConfiguration {

    @Bean
    public TokenAuthService tokenAuthService(JwtService jwtService) {
        return new TokenAuthService(jwtService);
    }

    @Bean
    public JwtService jwtService(@Autowired SecurityProperties securityProperties) {
        log.trace("Security properties {}", securityProperties);
        return new JwtService(Set.of(), Set.of(), securityProperties);
    }

}
