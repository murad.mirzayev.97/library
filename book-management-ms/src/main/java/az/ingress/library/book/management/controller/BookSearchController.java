package az.ingress.library.book.management.controller;

import az.ingress.library.book.management.domain.Books;
import az.ingress.library.book.management.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookSearchController {

    private final BookService bookService;

    @GetMapping("/list")
    public List<Books> bookList() {
        return bookService.getBookList();
    }

}
