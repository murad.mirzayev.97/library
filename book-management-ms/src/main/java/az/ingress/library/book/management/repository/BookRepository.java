package az.ingress.library.book.management.repository;

import az.ingress.library.book.management.domain.Books;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Books, Long>{

}
