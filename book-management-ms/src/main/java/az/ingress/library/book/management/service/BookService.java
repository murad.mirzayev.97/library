package az.ingress.library.book.management.service;

import az.ingress.library.book.management.domain.Books;

import java.util.List;

public interface BookService {

    public List<Books> getBookList();

}
