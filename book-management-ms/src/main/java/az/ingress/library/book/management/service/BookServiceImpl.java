package az.ingress.library.book.management.service;

import az.ingress.library.book.management.domain.Books;
import az.ingress.library.book.management.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService{

    private final BookRepository bookRepository;

    @Override
    public List<Books> getBookList() {
        return bookRepository.findAll();
    }
}
