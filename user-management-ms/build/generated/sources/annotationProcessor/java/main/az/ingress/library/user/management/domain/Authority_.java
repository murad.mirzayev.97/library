package az.ingress.library.user.management.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Authority.class)
public abstract class Authority_ {

	public static volatile SingularAttribute<Authority, String> authority;
	public static volatile SingularAttribute<Authority, Long> id;
	public static volatile SingularAttribute<Authority, User> user;

	public static final String AUTHORITY = "authority";
	public static final String ID = "id";
	public static final String USER = "user";

}

