package az.ingress.library.user.management.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ {

	public static volatile SingularAttribute<User, String> password;
	public static volatile SingularAttribute<User, Boolean> credentialsNonExpired;
	public static volatile SingularAttribute<User, Boolean> accountNonExpired;
	public static volatile SingularAttribute<User, Long> id;
	public static volatile SingularAttribute<User, String> email;
	public static volatile SetAttribute<User, Authority> authorities;
	public static volatile SingularAttribute<User, Boolean> enabled;
	public static volatile SingularAttribute<User, String> username;
	public static volatile SingularAttribute<User, Boolean> accountNonLocked;

	public static final String PASSWORD = "password";
	public static final String CREDENTIALS_NON_EXPIRED = "credentialsNonExpired";
	public static final String ACCOUNT_NON_EXPIRED = "accountNonExpired";
	public static final String ID = "id";
	public static final String EMAIL = "email";
	public static final String AUTHORITIES = "authorities";
	public static final String ENABLED = "enabled";
	public static final String USERNAME = "username";
	public static final String ACCOUNT_NON_LOCKED = "accountNonLocked";

}

