package az.ingress.library.user.management;

import az.ingress.library.user.management.domain.Authority;
import az.ingress.library.user.management.domain.User;
import az.ingress.library.user.management.repository.AuthorityRepository;
import az.ingress.library.user.management.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class UserManagementMsApplication implements CommandLineRunner {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;

    public static void main(String[] args) {
        SpringApplication.run(UserManagementMsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//        User user = User.builder()
//                .username("murad")
//                .password("12345")
//                .enabled(true)
//                .email("murad@gmail.com")
//                .build();
//
//        userRepository.save(user);
//
//        Authority authority = Authority.builder()
//                .authority("ROLE_ADMIN")
//                .build();
//
//        authority.setUser(user);
//
//
//        authorityRepository.save(authority);


    }
}
