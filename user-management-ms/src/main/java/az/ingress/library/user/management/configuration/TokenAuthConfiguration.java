package az.ingress.library.user.management.configuration;


import az.ingress.library.common.service.JwtService;
import az.ingress.library.common.service.TokenAuthService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TokenAuthConfiguration {

    @Bean
    public TokenAuthService tokenAuthService(JwtService jwtService) {
        return new TokenAuthService(jwtService);
    }
}
