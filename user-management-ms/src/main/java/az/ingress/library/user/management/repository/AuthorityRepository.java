package az.ingress.library.user.management.repository;

import az.ingress.library.user.management.domain.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
}
